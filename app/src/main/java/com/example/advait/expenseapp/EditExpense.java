package com.example.advait.expenseapp;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.advait.dao.Expense;
import com.example.advait.dao.ExpenseCategory;

import java.util.ArrayList;

public class EditExpense extends AppCompatActivity {

    private final static String TAG = "EditExpense";
    ArrayList<Expense> expenses = null;
   // String a[] = {"HelloWorld", "dssdgsf"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_expense);

        if (getIntent().getExtras() != null) {
            expenses = getIntent().getExtras().getParcelableArrayList(MainActivity.EXPENSE_LIST_KEY);
        }
        Log.d(TAG, "onCreate: "+expenses.size());
        String[] items = new String[expenses.size()];
        int i = 0;
        for (Expense e : expenses) {
            items[i++] = e.getName();
            Log.d(TAG, "onClick: " + e.getName());
        }

        String[] categories = new String[ExpenseCategory.values().length];
        int j = 0;
        for(ExpenseCategory expenseCategory : ExpenseCategory.values()){
            categories[j++]=expenseCategory.getDescription();
        }

        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, categories);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.spinner);
        sItems.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick an expense")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pouplateExpense(which);
                    }
                });

        final AlertDialog alert = builder.create();

        findViewById(R.id.editExpSelectExpensebutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.show();
            }
        });

    }

    private void pouplateExpense(int index) {
          Expense exp = expenses.get(index);

     //   ((EditText) findViewById(R.id.expenseNameeditText)).setText(items[index]);
        ((Spinner) findViewById(R.id.spinner)).setSelection(exp.getCategory().getId()-1);
        Log.d(TAG, "pouplateExpense: "+exp.getCategory().getId());
        // Log.d("EditExpense",exp.getName());
        ((EditText) findViewById(R.id.expenseNameeditText)).setText(""+exp.getName());
         ((EditText) findViewById(R.id.amountEditText)).setText(""+exp.getAmount());
        ((EditText) findViewById(R.id.dateEditText)).setText(""+exp.getMonth()+"/"+exp.getDay()+"/"+exp.getYear());
    }
}
