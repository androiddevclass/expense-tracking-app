package com.example.advait.expenseapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.advait.dao.Expense;
import com.example.advait.dao.ExpenseCategory;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String EXPENSE_LIST_KEY="allExpenses";
    public static final int EXPENSE_INDEX_TO_DEAL=-1;
    private static final String TAG = "MainActivity";
    public final static int REQ_CODE = 100;


    private ArrayList<Expense> expenses = new ArrayList<Expense>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initListners();
    }



    private void initListners() {
        findViewById(R.id.editExpenseButton).setOnClickListener(clickListener);
        findViewById(R.id.addExpenseButton).setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case R.id.editExpenseButton:
                    Log.d(TAG, "onClick: editAct opening");
                    /*Expense e = new Expense("demo", ExpenseCategory.GROCERIES, 520.2d, 4,5,6);
                    Expense e2 = new Expense("demo2", ExpenseCategory.OTHER, 50.2d, 4,5,622);
                    expenses.add(e);
                    expenses.add(e2);*/
                    Log.d("MainActivity",""+expenses.size());
                    Intent intent = new Intent(getBaseContext(), EditExpense.class);
                    intent.putParcelableArrayListExtra(EXPENSE_LIST_KEY, expenses);
                    startActivity(intent);
                    break;
                case R.id.addExpenseButton:
                    Log.d(TAG, "onClick: AddAct opening");
                    Intent intent_add = new Intent(getBaseContext(), AddExpense.class);
                    intent_add.putParcelableArrayListExtra(EXPENSE_LIST_KEY, expenses);
                    startActivityForResult(intent_add, REQ_CODE);

                    break;
            }
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Expense expense = (Expense)data.getParcelableExtra(EXPENSE_LIST_KEY);
                expenses.add(expense);
                Log.d(TAG, "onActivityResult: "+expense.toString());
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("demo","cancelled");
            }
        }

    }

}
