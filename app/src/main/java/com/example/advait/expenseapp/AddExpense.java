package com.example.advait.expenseapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.advait.dao.Expense;
import com.example.advait.dao.ExpenseCategory;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class AddExpense extends AppCompatActivity {
    ArrayList<Expense> expenses = null;
    private static final String TAG = "AddExpense";
    int day,month,year;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);

        if (getIntent().getExtras() != null) {
            expenses = getIntent().getExtras().getParcelableArrayList(MainActivity.EXPENSE_LIST_KEY);
        }


        Spinner spinner = (Spinner) findViewById(R.id.add_spinner);

        String[] categories = new String[ExpenseCategory.values().length];
        int j = 0;
        for (ExpenseCategory expenseCategory : ExpenseCategory.values()) {
            categories[j++] = expenseCategory.getDescription();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, categories);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.add_spinner);
        sItems.setAdapter(adapter);

        findViewById(R.id.add_dateEditText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.add_datePicker).setVisibility(View.VISIBLE);
                Log.d(TAG, "onClick: datepicker visible");
            }
        });
        Log.d(TAG, "onCreate: listner set to edit text");

        day=month=year=-1;

        GregorianCalendar calendar = new GregorianCalendar();
        ((DatePicker)findViewById(R.id.add_datePicker)).init(calendar.get(GregorianCalendar.YEAR), calendar.get(GregorianCalendar.MONTH), calendar.get(GregorianCalendar.DATE), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                DatePicker picker = ((DatePicker)findViewById(R.id.add_datePicker));
                day=view.getDayOfMonth();
                month=view.getMonth();
                year=view.getYear();
                ((EditText)findViewById(R.id.add_dateEditText)).setText(month+"/"+day+"/"+year);
                picker.setVisibility(View.INVISIBLE);
            }
        });

        findViewById(R.id.addExpenseButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("OnClickExpense", "Expense Button Clicked");
                String name = ((EditText) findViewById(R.id.add_expenseNameeditText)).getText().toString();
                Spinner s = ((Spinner) findViewById(R.id.add_spinner));
                long i = s.getSelectedItemId();
                i++;
                int j = (int) i;
                Log.d("check category", "onClick: " + i);
                ExpenseCategory category = ExpenseCategory.getExpenseCategoryFromId(j);

                EditText e = ((EditText) findViewById(R.id.add_amountEditText));
                Double amount = (Double.parseDouble(e.getText().toString()));

                Expense expense = new Expense(name, category, amount, day, month, year);

                Intent intent = new Intent();
                intent.putExtra(MainActivity.EXPENSE_LIST_KEY,expense);
                setResult(RESULT_OK,intent);


                Toast.makeText(AddExpense.this, "Expense Added", Toast.LENGTH_SHORT).show();
                Log.d("Expense", expense.toString());
            }
        });
    }
}
