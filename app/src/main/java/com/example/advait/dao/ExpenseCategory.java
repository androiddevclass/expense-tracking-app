package com.example.advait.dao;

import java.io.Serializable;

/**
 * Created by deva on 9/10/16.
 */
public enum ExpenseCategory {
    GROCERIES(1,"Groceries"),
    INVOICE(2, "Invoice"),
    TRANSPORTATION(3, "Transportation"),
    SHOPPING(4,"Shopping"),
    RENT(5,"Rent"),
    TRIPS(6,"Trips"),
    UTILITIES(7,"Utilities"),
    OTHER(8,"Other");

    int id;
    String description;

    @Override
    public String toString() {
        return description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    ExpenseCategory(int id, String description){
        this.id=id;
        this.description=description;
    }

    public static ExpenseCategory getExpenseCategoryFromId(int id){
        switch (id){
            case 1:
                return GROCERIES;
            case 2:
                return INVOICE;
            case 3:
                return TRANSPORTATION;
            case 4:
                return SHOPPING;
            case 5:
                return RENT;
            case 6:
                return TRIPS;
            case 7:
                return UTILITIES;
            case 8:
                return OTHER;
        }
       return null;
    }
}
