package com.example.advait.dao;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by deva on 9/10/16.
 */
public class Expense implements Parcelable {

    String name;
    ExpenseCategory category;
    double amount;
    int day, month, year;
    Image bill;

    public Expense(String name, ExpenseCategory category, double amount, int day, int month, int year) {
        this.name = name;
        this.category = category;
        this.amount = amount;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    protected Expense(Parcel in) {
        name = in.readString();
        category = ExpenseCategory.getExpenseCategoryFromId(in.readInt());
        amount = in.readDouble();
        day = in.readInt();
        month = in.readInt();
        year = in.readInt();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExpenseCategory getCategory() {
        return category;
    }

    public void setCategory(ExpenseCategory category) {
        this.category = category;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Image getBill() {
        return bill;
    }

    public void setBill(Image bill) {
        this.bill = bill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Expense expense = (Expense) o;

        if (Double.compare(expense.amount, amount) != 0) return false;
        if (day != expense.day) return false;
        if (month != expense.month) return false;
        if (year != expense.year) return false;
        if (!name.equals(expense.name)) return false;
        return category == expense.category;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name.hashCode();
        result = 31 * result + category.hashCode();
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + day;
        result = 31 * result + month;
        result = 31 * result + year;
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(category.getId());
        dest.writeDouble(amount);
        dest.writeInt(day);
        dest.writeInt(month);
        dest.writeInt(year);
    }

//    public static final Parcelable.Creator<Expense> expCreator = new Parcelable.Creator<Expense>(){
//
//        @Override
//        public Expense createFromParcel(Parcel source) {
//            Expense e = new Expense();
//            e.setName(source.readString());
//            e.setCategory(ExpenseCategory.getExpenseCategoryFromId(source.readInt()));
//            e.setAmount(source.readDouble());
//            e.setDay(source.readInt());
//            e.setMonth(source.readInt());
//            e.setYear(source.readInt());
//            //e.setBill(source.reaI);
//            return e;
//        }
//
//        @Override
//        public Expense[] newArray(int size) {
//            return new Expense[size];
//        }
//    };

    public static final Creator<Expense> CREATOR = new Creator<Expense>() {
        @Override
        public Expense createFromParcel(Parcel in) {
            return new Expense(in);
        }

        @Override
        public Expense[] newArray(int size) {
            return new Expense[size];
        }
    };
}
